# Evalest Midiator Keyboard

### A WPF C# application that allows the user to control the volume of windows applications by using the keyboard and mouse.

Shows a list of available audio apps and recording devices. 
The user can assign a keyboard shortcut to each app and the configuration is saved into a .config file. 
Then by holding the shortcut and using the mouse wheel, the user can increase or lower the volume of this app.
Finally, the .config file can be shared amongst different users.