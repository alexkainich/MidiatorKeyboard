﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EvalestMidiatorKeyboard
{
    public class KeyboardControl
    {
        private const int WH_KEYBOARD_LL = 13;
        private const int WM_KEYDOWN = 0x0100;
        private const int WM_KEYUP = 0x0101;
        private const int WM_SYSKEYDOWN = 0x0104;
        private const int WM_SYSKEYUP = 0x0105;
        public static string keyCombo;
        public static List<Keys> sameKeys = new List<Keys>();
        public static List<Keys> pressedKeys = new List<Keys>();

        public static event EventHandler<KeyboardEventArgs> KeyboardAction = delegate { };

        private static LowLevelKeyboardProc _proc = HookCallback;
        private static IntPtr _hookID = IntPtr.Zero;

        public static void Start()
        {
            _hookID = SetHook(_proc);
        }

        public static void Stop()
        {
            UnhookWindowsHookEx(_hookID);
        }

        private static IntPtr SetHook(LowLevelKeyboardProc proc)
        {
            using (Process curProcess = Process.GetCurrentProcess())
            using (ProcessModule curModule = curProcess.MainModule)
            {
                IntPtr hook = SetWindowsHookEx(WH_KEYBOARD_LL, proc, GetModuleHandle("user32"), 0);
                if (hook == IntPtr.Zero) throw new System.ComponentModel.Win32Exception();
                return hook;
            }
        }

        private delegate IntPtr LowLevelKeyboardProc(int nCode, IntPtr wParam, IntPtr lParam);
        private static IntPtr HookCallback(int nCode, IntPtr wParam, IntPtr lParam)
        {
            if (nCode >= 0) //if we receive keyboard signal
            {
                int vkCode = Marshal.ReadInt32(lParam);
                if ((wParam == (IntPtr)WM_KEYDOWN || wParam == (IntPtr)WM_SYSKEYDOWN) && pressedKeys.Count < 4)
                {
                    if (!sameKeys.Contains((Keys)vkCode))
                    {
                        pressedKeys.Add((Keys)vkCode);
                        sameKeys.Add((Keys)vkCode);
                        switch (pressedKeys.Count)
                        {
                            case 0:
                                Console.WriteLine("This should never appear!");
                                break;
                            case 1:
                                keyCombo = pressedKeys[0].ToString();
                                break;
                            case 2:
                                keyCombo = pressedKeys[0].ToString() + "+" + pressedKeys[1].ToString();
                                break;
                            case 3:
                                keyCombo = pressedKeys[0].ToString() + "+" + pressedKeys[1].ToString() + "+" + pressedKeys[2].ToString();
                                break;
                            case 4:
                                keyCombo = pressedKeys[0].ToString() + "+" + pressedKeys[1].ToString() + "+" + pressedKeys[2].ToString() + "+" + pressedKeys[3].ToString();
                                break;
                        }
                        Console.WriteLine("playing:" + keyCombo);
                        keyCombo = "playing:" + keyCombo;
                        KeyboardAction(null, new KeyboardEventArgs(keyCombo));
                        keyCombo = "";
                    }
                }
                else if ((wParam == (IntPtr)WM_KEYUP || wParam == (IntPtr)WM_SYSKEYUP) && pressedKeys.Contains((Keys)vkCode))
                {
                    switch (pressedKeys.Count)
                    {
                        case 0:
                            return CallNextHookEx(_hookID, nCode, wParam, lParam);
                        case 1:
                            keyCombo = pressedKeys[0].ToString();
                            break;
                        case 2:
                            keyCombo = pressedKeys[0].ToString() + "+" + pressedKeys[1].ToString();
                            break;
                        case 3:
                            keyCombo = pressedKeys[0].ToString() + "+" + pressedKeys[1].ToString() + "+" + pressedKeys[2].ToString();
                            break;
                        case 4:
                            keyCombo = pressedKeys[0].ToString() + "+" + pressedKeys[1].ToString() + "+" + pressedKeys[2].ToString() + "+" + pressedKeys[3].ToString();
                            break;
                    }
                    Console.WriteLine("recording:" + keyCombo);
                    keyCombo = "recording:" + keyCombo;
                    KeyboardAction(null, new KeyboardEventArgs(keyCombo));
                    keyCombo = "";
                    pressedKeys.Clear();
                    sameKeys.Clear();
                }
            }
            return CallNextHookEx(_hookID, nCode, wParam, lParam);
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr SetWindowsHookEx(int idHook, LowLevelKeyboardProc lpfn, IntPtr hMod, uint dwThreadId);
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool UnhookWindowsHookEx(IntPtr hhk);
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode, IntPtr wParam, IntPtr lParam);
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetModuleHandle(string lpModuleName);
    }

    public class KeyboardEventArgs : EventArgs
    {
        public KeyboardEventArgs(string keyCombo)
        {
            this.KeyCombo = keyCombo;
        }
        public string KeyCombo { get; private set; }
    }
}
