﻿#pragma checksum "..\..\MainWindow.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "AAD55A4FCDA5BD1A8E17D70BDFB62C937C3483DD"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using EvalestMidiatorKeyboard;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace EvalestMidiatorKeyboard {
    
    
    /// <summary>
    /// MainWindow
    /// </summary>
    public partial class MainWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 1 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal EvalestMidiatorKeyboard.MainWindow MainWindow1;
        
        #line default
        #line hidden
        
        
        #line 102 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid Main;
        
        #line default
        #line hidden
        
        
        #line 138 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Ellipse Close_btn;
        
        #line default
        #line hidden
        
        
        #line 143 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Ellipse Min_btn;
        
        #line default
        #line hidden
        
        
        #line 148 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Ellipse Max_btn;
        
        #line default
        #line hidden
        
        
        #line 150 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button recordControl;
        
        #line default
        #line hidden
        
        
        #line 151 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button removeControl;
        
        #line default
        #line hidden
        
        
        #line 152 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox checkStart;
        
        #line default
        #line hidden
        
        
        #line 153 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label resultText;
        
        #line default
        #line hidden
        
        
        #line 154 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView activeApps;
        
        #line default
        #line hidden
        
        
        #line 162 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView activeMics;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/EvalestMidiatorKeyboard;component/mainwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\MainWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.MainWindow1 = ((EvalestMidiatorKeyboard.MainWindow)(target));
            
            #line 8 "..\..\MainWindow.xaml"
            this.MainWindow1.Closing += new System.ComponentModel.CancelEventHandler(this.OnClose);
            
            #line default
            #line hidden
            
            #line 8 "..\..\MainWindow.xaml"
            this.MainWindow1.StateChanged += new System.EventHandler(this.OnStateChanged);
            
            #line default
            #line hidden
            
            #line 8 "..\..\MainWindow.xaml"
            this.MainWindow1.IsVisibleChanged += new System.Windows.DependencyPropertyChangedEventHandler(this.OnIsVisibleChanged);
            
            #line default
            #line hidden
            return;
            case 2:
            this.Main = ((System.Windows.Controls.Grid)(target));
            return;
            case 3:
            
            #line 121 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.Grid)(target)).MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.move_window);
            
            #line default
            #line hidden
            return;
            case 4:
            
            #line 134 "..\..\MainWindow.xaml"
            ((System.Windows.Shapes.Rectangle)(target)).MouseEnter += new System.Windows.Input.MouseEventHandler(this.Activate_Title_Icons);
            
            #line default
            #line hidden
            
            #line 134 "..\..\MainWindow.xaml"
            ((System.Windows.Shapes.Rectangle)(target)).MouseLeave += new System.Windows.Input.MouseEventHandler(this.Deactivate_Title_Icons);
            
            #line default
            #line hidden
            return;
            case 5:
            this.Close_btn = ((System.Windows.Shapes.Ellipse)(target));
            
            #line 136 "..\..\MainWindow.xaml"
            this.Close_btn.MouseLeftButtonUp += new System.Windows.Input.MouseButtonEventHandler(this.EXIT);
            
            #line default
            #line hidden
            
            #line 137 "..\..\MainWindow.xaml"
            this.Close_btn.MouseEnter += new System.Windows.Input.MouseEventHandler(this.Activate_Title_Icons);
            
            #line default
            #line hidden
            
            #line 137 "..\..\MainWindow.xaml"
            this.Close_btn.MouseLeave += new System.Windows.Input.MouseEventHandler(this.Deactivate_Title_Icons);
            
            #line default
            #line hidden
            
            #line 138 "..\..\MainWindow.xaml"
            this.Close_btn.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.Close_pressing);
            
            #line default
            #line hidden
            return;
            case 6:
            this.Min_btn = ((System.Windows.Shapes.Ellipse)(target));
            
            #line 141 "..\..\MainWindow.xaml"
            this.Min_btn.MouseLeftButtonUp += new System.Windows.Input.MouseButtonEventHandler(this.MINIMIZE);
            
            #line default
            #line hidden
            
            #line 142 "..\..\MainWindow.xaml"
            this.Min_btn.MouseEnter += new System.Windows.Input.MouseEventHandler(this.Activate_Title_Icons);
            
            #line default
            #line hidden
            
            #line 142 "..\..\MainWindow.xaml"
            this.Min_btn.MouseLeave += new System.Windows.Input.MouseEventHandler(this.Deactivate_Title_Icons);
            
            #line default
            #line hidden
            
            #line 143 "..\..\MainWindow.xaml"
            this.Min_btn.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.Min_pressing);
            
            #line default
            #line hidden
            return;
            case 7:
            this.Max_btn = ((System.Windows.Shapes.Ellipse)(target));
            
            #line 146 "..\..\MainWindow.xaml"
            this.Max_btn.MouseLeftButtonUp += new System.Windows.Input.MouseButtonEventHandler(this.MAX_RESTORE);
            
            #line default
            #line hidden
            
            #line 147 "..\..\MainWindow.xaml"
            this.Max_btn.MouseEnter += new System.Windows.Input.MouseEventHandler(this.Activate_Title_Icons);
            
            #line default
            #line hidden
            
            #line 147 "..\..\MainWindow.xaml"
            this.Max_btn.MouseLeave += new System.Windows.Input.MouseEventHandler(this.Deactivate_Title_Icons);
            
            #line default
            #line hidden
            
            #line 148 "..\..\MainWindow.xaml"
            this.Max_btn.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.Max_pressing);
            
            #line default
            #line hidden
            return;
            case 8:
            this.recordControl = ((System.Windows.Controls.Button)(target));
            
            #line 150 "..\..\MainWindow.xaml"
            this.recordControl.Click += new System.Windows.RoutedEventHandler(this.recordControl_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.removeControl = ((System.Windows.Controls.Button)(target));
            
            #line 151 "..\..\MainWindow.xaml"
            this.removeControl.Click += new System.Windows.RoutedEventHandler(this.RemoveControl_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.checkStart = ((System.Windows.Controls.CheckBox)(target));
            
            #line 152 "..\..\MainWindow.xaml"
            this.checkStart.Checked += new System.Windows.RoutedEventHandler(this.CheckBox_Checked);
            
            #line default
            #line hidden
            
            #line 152 "..\..\MainWindow.xaml"
            this.checkStart.Unchecked += new System.Windows.RoutedEventHandler(this.CheckBox_Unchecked);
            
            #line default
            #line hidden
            return;
            case 11:
            this.resultText = ((System.Windows.Controls.Label)(target));
            return;
            case 12:
            this.activeApps = ((System.Windows.Controls.ListView)(target));
            
            #line 154 "..\..\MainWindow.xaml"
            this.activeApps.MouseUp += new System.Windows.Input.MouseButtonEventHandler(this.activeApps_MouseUp);
            
            #line default
            #line hidden
            return;
            case 13:
            this.activeMics = ((System.Windows.Controls.ListView)(target));
            
            #line 162 "..\..\MainWindow.xaml"
            this.activeMics.MouseUp += new System.Windows.Input.MouseButtonEventHandler(this.activeMics_MouseUp);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

