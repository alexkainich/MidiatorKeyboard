﻿using System;
using System.Windows;
using System.Diagnostics;
using System.Windows.Forms;
using System.Windows.Controls;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Linq;
using System.Threading;
using System.Collections.ObjectModel;
using CSCore.CoreAudioAPI;
using NAudio.Wave;
using NAudio.Mixer;
using System.IO;
using Microsoft.Win32;
using System.Windows.Input;
using System.Windows.Media;
using System.ComponentModel;
using System.Windows.Interop;
using System.Collections.Specialized;

namespace EvalestMidiatorKeyboard
{
    public partial class MainWindow : Window
    {
        private System.Windows.Forms.NotifyIcon m_notifyIcon;
        private WindowState m_storedWindowState = WindowState.Normal;
        public static System.Windows.Forms.Timer timer1; //used for recording
        public static System.Windows.Forms.Timer timer2; //used for "listeningForSessions"
        public static int recording = 0;
        public int recordingTime;
        public int AppOrMic = 0; //0: no selection, 1: App, 2: Mic
        public recordingLabel recLl = new recordingLabel();
        public List<int> appsToVolume = new List<int>();
        public List<int> micsToVolume = new List<int>();
        public List<string> sessionNames = new List<string>();
        public List<string> sessionControls = new List<string>();
        public List<NAudio.CoreAudioApi.SimpleAudioVolume> sessionVolume = new List<NAudio.CoreAudioApi.SimpleAudioVolume>();
        public List<string> recordingDevices = new List<string>();
        public List<string> recordingControls = new List<string>();
        public ObservableCollection<App> activeAppsChanges = new ObservableCollection<App>();
        public ObservableCollection<Mic> activeMicsChanges = new ObservableCollection<Mic>();
        public string exePath;
        public NAudio.CoreAudioApi.AudioSessionManager sessionManager2;
        public NAudio.CoreAudioApi.MMDevice defaultdevice;

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;
        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public MainWindow()
        {
            InitializeComponent();
            m_notifyIcon = new System.Windows.Forms.NotifyIcon();
            Stream iconStream = System.Windows.Application.GetResourceStream(new Uri("pack://application:,,,/Images/midiatorkeyboard.ico")).Stream;
            m_notifyIcon.Icon = new System.Drawing.Icon(iconStream);
            m_notifyIcon.Click += new EventHandler(m_notifyIcon_Click);
            startMinimized();

            resultText.DataContext = recLl; // This is the whole bind operation
            alreadyRunning();
            findExePath();
            WindowState = WindowState.Minimized;
            clearAllLists();

            sessionNames.Add("MasterVolume");
            sessionControls.Add("");

            findSessions();

            findRecordingDevices();
            populateActiveApps();
            readConfigFile(); //read config file
            startUpCheck();
            listenForSessions();

            MouseControl.Start();
            MouseControl.MouseAction += new EventHandler<MyMouseEventArgs>(mouseReceived);
            KeyboardControl.Start();
            KeyboardControl.KeyboardAction += new EventHandler<KeyboardEventArgs>(keyReceived);
        }

        private void startMinimized()
        {
            WindowState = WindowState.Minimized;
            m_notifyIcon.Visible = true;

            Hide();
        }

        private void OnClose(object sender, CancelEventArgs args)
        {
            m_notifyIcon.Dispose();
            m_notifyIcon = null;
        }

        private void OnStateChanged(object sender, EventArgs args)
        {
            if (WindowState == WindowState.Minimized)
            {
                Hide();
            }
            else
            {
                m_storedWindowState = WindowState;
            }
        }

        void OnIsVisibleChanged(object sender, DependencyPropertyChangedEventArgs args)
        {
            CheckTrayIcon();
        }

        void m_notifyIcon_Click(object sender, EventArgs e)
        {
            Show();
            WindowState = m_storedWindowState;
        }

        void CheckTrayIcon()
        {
            ShowTrayIcon(!IsVisible);
        }

        void ShowTrayIcon(bool show)
        {
            if (m_notifyIcon != null)
                m_notifyIcon.Visible = show;
        }

        public void OnPowerChange(object s, PowerModeChangedEventArgs e)
        {
            Process self = Process.GetCurrentProcess();
            switch (e.Mode)
            {
                case PowerModes.Resume:
                    RestartApp(self.Id, "EvalestMidiatorKeyboard");
                    break;
                case PowerModes.Suspend:
                    break;
            }
        }

        static void RestartApp(int pid, string applicationName)
        {
            try
            {
                System.Diagnostics.Process.Start(System.Windows.Application.ResourceAssembly.Location);
                System.Windows.Application.Current.Shutdown();
            }
            catch
            {

            }
        }

        public class App : INotifyPropertyChanged
        {
            private string application;
            public string Application
            {
                get { return this.application; }
                set
                {
                    if (this.application != value)
                    {
                        this.application = value;
                        this.NotifyPropertyChanged("Application");
                    }
                }
            }

            private string akey;
            public string AKey
            {
                get { return this.akey; }
                set
                {
                    if (this.akey != value)
                    {
                        this.akey = value;
                        this.NotifyPropertyChanged("AKey");
                    }
                }
            }

            public event PropertyChangedEventHandler PropertyChanged;

            public void NotifyPropertyChanged(string propName)
            {
                if (this.PropertyChanged != null)
                    this.PropertyChanged(this, new PropertyChangedEventArgs(propName)); //notifies the listview
            }
        }

        public class Mic : INotifyPropertyChanged
        {
            private string microphone;
            public string Microphone
            {
                get { return this.microphone; }
                set
                {
                    if (this.microphone != value)
                    {
                        this.microphone = value;
                        this.NotifyPropertyChanged("Microphone");
                    }
                }
            }

            private string mkey;
            public string MKey
            {
                get { return this.mkey; }
                set
                {
                    if (this.mkey != value)
                    {
                        this.mkey = value;
                        this.NotifyPropertyChanged("MKey");
                    }
                }
            }

            public event PropertyChangedEventHandler PropertyChanged;

            public void NotifyPropertyChanged(string propName)
            {
                if (this.PropertyChanged != null)
                    this.PropertyChanged(this, new PropertyChangedEventArgs(propName)); //notifies the listview ?
            }
        }

        public class recordingLabel : INotifyPropertyChanged
        {
            private string _name;
            public string Name
            {
                get { return this._name; }
                set
                {
                    if (this._name != value)
                    {
                        this._name = value;
                        this.OnPropertyChanged("Name");
                    }
                }
            }

            public event PropertyChangedEventHandler PropertyChanged;

            public void OnPropertyChanged(string propertyName)
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void keyReceived(object sender, KeyboardEventArgs e)
        {
            if (recording == 1)
            {
                String[] substrings = e.KeyCombo.Split(':');
                if (substrings[0] == "recording")
                {
                    if (AppOrMic == 0)
                    {
                        recLl.Name = "";
                        return;
                    }
                    if (AppOrMic == 1)
                    {
                        for (int i = 0; i < activeAppsChanges.Count; i++)
                        {
                            if (activeAppsChanges[i].Application == (activeApps.SelectedItem as App).Application)
                            {
                                (activeApps.Items[i] as App).AKey = substrings[1];
                                sessionControls[i] = substrings[1];
                            }
                        }
                        recLl.Name = "";
                    }
                    else if (AppOrMic == 2)
                    {
                        for (int i = 0; i < activeMicsChanges.Count; i++)
                        {
                            if (activeMicsChanges[i].Microphone == (activeMics.SelectedItem as Mic).Microphone)
                            {
                                (activeMics.Items[i] as Mic).MKey = substrings[1];
                                recordingControls[i] = substrings[1];
                            }
                        }
                        recLl.Name = "";
                    }
                    recording = 0;
                    recLl.Name = "";
                }
            }
            else
            {
                String[] substrings = e.KeyCombo.Split(':');
                if (substrings[0] == "playing") //allow the user to roll the mouse
                {
                    for (int i = 0; i < activeAppsChanges.Count; i++)
                    {
                        if (activeAppsChanges[i].AKey == substrings[1])
                        {
                            appsToVolume.Add(i); //we have found the indicies of the APPS we have to change by rolling the mouse!
                        }
                    }
                    for (int i = 0; i < activeMicsChanges.Count; i++)
                    {
                        if (activeMicsChanges[i].MKey == substrings[1])
                        {
                            micsToVolume.Add(i); //we have found the indicies of the MICS we have to change by rolling the mouse!
                        }
                    }
                }
                else if (substrings[0] == "recording") //stop the user from using the roll mouse to change volume
                {
                    appsToVolume.Clear();
                    micsToVolume.Clear();
                }
            }

        }
        private void mouseReceived(object sender, MyMouseEventArgs e)
        {
            if (e.MouseCombo == "up")
            {
                foreach (int i in appsToVolume)
                {
                    if (sessionNames[i] == "MasterVolume")
                    {
                        try
                        {
                            defaultdevice.AudioEndpointVolume.MasterVolumeLevelScalar += 0.03f;
                        }
                        catch
                        {
                            Console.WriteLine("Error in setting the master volume!");
                        }
                    }
                    else
                    {
                        try
                        {
                            sessionVolume[i - 1].Volume += 0.03f;
                        }
                        catch
                        {
                        }
                    }
                }
                foreach (int i in micsToVolume)
                {
                    try
                    {
                        setMicGain(i, 3f);
                    }
                    catch
                    {
                    }
                }
            }
            else
            {
                foreach (int i in appsToVolume)
                {
                    if (sessionNames[i] == "MasterVolume")
                    {
                        try
                        {

                            defaultdevice.AudioEndpointVolume.MasterVolumeLevelScalar -= 0.03f;
                        }
                        catch
                        {
                            Console.WriteLine("Error in setting the master volume!");
                        }
                    }
                    else
                    {
                        try
                        {
                            sessionManager2.Sessions[i - 1].SimpleAudioVolume.Volume -= 0.03f;
                        }
                        catch
                        {
                        }
                    }
                }
                foreach (int i in micsToVolume)
                {
                    try
                    {
                        setMicGain(i, -3f);
                    }
                    catch
                    {
                    }
                }
            }
        }

        public void setMicGain(int whichMic, double micGain) //not sure the app works if you use whichMic != 0
        {
            int waveInDeviceNumber = whichMic; //0 is the Default
            var mixerLine = new MixerLine((IntPtr)waveInDeviceNumber, 0, MixerFlags.WaveIn);
            bool micIsMuted = false;

            foreach (var control in mixerLine.Controls)
            {
                if (control.ControlType == MixerControlType.Volume)
                {
                    var volumeControl = control as UnsignedMixerControl;
                    volumeControl.Percent = Math.Min(100.0f, Math.Max(0.0f, volumeControl.Percent + micGain));
                    if (volumeControl.Percent == 0)
                    {
                        micIsMuted = true;
                    }
                    break;
                }
            }
            foreach (var control in mixerLine.Controls)
            {
                if (control.ControlType == MixerControlType.Mute)
                {
                    var muteControl = control as BooleanMixerControl;
                    if (micIsMuted == true)
                    {
                        muteControl.Value = true;
                    }
                    else
                    {
                        muteControl.Value = false;
                    }
                    break;
                }
            }
        }

        private void listenForSessions()
        {
            timer2 = new System.Windows.Forms.Timer();
            timer2.Tick += new EventHandler(reloadApps);
            timer2.Interval = 5000; // in miliseconds
            timer2.Start();
        }

        private void alreadyRunning()
        {
            Process self = Process.GetCurrentProcess();
            if (Process.GetProcessesByName("EvalestMidiatorKeyboard").Count() > 1)
            {
                foreach (var process in Process.GetProcessesByName("EvalestMidiatorKeyboard"))
                {
                    if (self.Id != process.Id)
                    {
                        process.Kill();
                    }
                }
            }
        }

        public void findSessions()
        {
            try
            {
                NAudio.CoreAudioApi.MMDeviceEnumerator devEnum = new NAudio.CoreAudioApi.MMDeviceEnumerator();
                defaultdevice = devEnum.GetDefaultAudioEndpoint(NAudio.CoreAudioApi.DataFlow.Render, NAudio.CoreAudioApi.Role.Multimedia);
                sessionManager2 = defaultdevice.AudioSessionManager;
            }
            catch
            {
                Console.WriteLine("Error in sessionManager!");
                return;
            }

            for (int i = 0; i < sessionManager2.Sessions.Count; i++)
            {
                try
                {
                    int procNum = (int)sessionManager2.Sessions[i].GetProcessID;
                    string tempName = Process.GetProcessById(procNum).ProcessName.ToString();
                    sessionNames.Add(tempName); //we save the session names in this list
                    sessionVolume.Add(sessionManager2.Sessions[i].SimpleAudioVolume);
                    sessionControls.Add("");
                }
                catch
                {
                    Console.WriteLine("Error in adding the session. Perhaps the session was closed while the method was running?");
                    continue;
                }

            }
        }

        public void populateActiveApps()
        {
            foreach (String sname in sessionNames)
            {
                activeAppsChanges.Add(new App { Application = sname, AKey = "" }); //we populate the listview in the windows form
            }
            activeApps.ItemsSource = activeAppsChanges;
        }

        public void findRecordingDevices()
        {
            int waveInDevices = WaveIn.DeviceCount;

            for (int waveInDevice = 0; waveInDevice < waveInDevices; waveInDevice++)
            {
                WaveInCapabilities deviceInfo = WaveIn.GetCapabilities(waveInDevice);
                activeMicsChanges.Add(new Mic { Microphone = deviceInfo.ProductName, MKey = "none" });
                recordingDevices.Add(deviceInfo.ProductName);
                recordingControls.Add("");
            }
            activeMics.ItemsSource = activeMicsChanges;
        }

        public void clearAllLists()
        {
            sessionNames.Clear();
            sessionControls.Clear();
            sessionVolume.Clear();
            recordingDevices.Clear();
            recordingControls.Clear();
        }

        public void readConfigFile()
        {
            try
            {
                var currentConfigs = EvalestMidiatorKeyboard.Properties.Settings.Default.Config;

                if (currentConfigs == null)
                {
                    return;
                }

                for (int i = 0; i < currentConfigs.Count; i++)
                {
                    String[] substrings = currentConfigs[i].Split(':'); //1: session name , 2: control
                    for (int j = 0; j < sessionNames.Count; j++)
                    {
                        if (substrings[0] == sessionNames[j])
                        {
                            sessionControls[j] = substrings[1];
                            (activeApps.Items[j] as App).AKey = sessionControls[j];
                        }
                    }
                    for (int j = 0; j < recordingDevices.Count; j++)
                    {
                        if (substrings[0] == recordingDevices[j])
                        {
                            recordingControls[j] = substrings[1];
                            (activeMics.Items[j] as Mic).MKey = recordingControls[j];
                        }
                    }
                }
            }
            catch
            {
            }
        }

        private void userSelection()
        {
            AppOrMic = 0;
            if (activeApps.SelectedItems.Count > 0)
            {
                AppOrMic = 1;
            }
            else if (activeMics.SelectedItems.Count > 0)
            {
                AppOrMic = 2;
            }
        }

        private void reloadApps(object sender, EventArgs e)
        {
            string tempNme = "";
            userSelection();
            if (AppOrMic == 1)
            {
                tempNme = ((App)activeApps.SelectedItem).Application;
            }
            else if (AppOrMic == 2)
            {
                tempNme = ((Mic)activeMics.SelectedItem).Microphone;
            }

            SaveConfig();
            activeAppsChanges.Clear();
            activeMicsChanges.Clear();
            clearAllLists();
            findExePath();

            sessionNames.Add("MasterVolume");
            sessionControls.Add("");

            findSessions();

            findRecordingDevices();
            populateActiveApps();
            readConfigFile(); //populate controls

            if (AppOrMic == 1)
            {
                for (int i = 0; i < activeApps.Items.Count; i++)
                {
                    if ((activeApps.Items[i] as App).Application == tempNme)
                    {
                        activeApps.SelectedIndex = i;
                        break;
                    }
                }
            }
            else if (AppOrMic == 2)
            {
                for (int i = 0; i < activeMics.Items.Count; i++)
                {
                    if ((activeMics.Items[i] as Mic).Microphone == tempNme)
                    {
                        activeMics.SelectedIndex = i;
                        break;
                    }

                }
            }
        }

        private void recordControl_Click(object sender, EventArgs e)
        {
            if (AppOrMic != 0)
            {
                recording = 1;
                recordingTime = 5;
                updateRecLabel(sender, e); //start timer AND update label (bottom left corner in the window)
            }
        }

        private void updateRecLabel(object sender, EventArgs e)
        {
            if (timer1 != null)
                timer1.Stop();

            if (recordingTime == 0 && recording == 1)
            {
                recording = 0;
                recLl.Name = "";
            }
            if (recording == 1)
            {
                recLl.Name = "Waiting for key input " + recordingTime + "...";
                recordingTime--;
                timer1 = new System.Windows.Forms.Timer();
                timer1.Tick += new EventHandler(updateRecLabel);
                timer1.Interval = 1000; // in miliseconds
                timer1.Start();
            }

        }

        private void SaveConfig()
        {
            var currentConfigs = EvalestMidiatorKeyboard.Properties.Settings.Default.Config;

            var newConfigs = new StringCollection();

            // Save in use application configs
            for (int i = 0; i < sessionNames.Count; i++)
            {
                if (sessionNames[i] != null && sessionControls[i] != null)
                {
                    newConfigs.Add(sessionNames[i] + ":" + sessionControls[i]);
                }
            }

            // Save in use recording configs
            for (int i = 0; i < recordingDevices.Count; i++)
            {
                if (recordingDevices[i] != null && recordingControls[i] != null)
                {
                    newConfigs.Add(recordingDevices[i] + ":" + recordingControls[i]);
                }
            }

            // Save all other configs not currently in use
            if (currentConfigs != null)
            {
                foreach (string config in currentConfigs)
                {
                    String configName = config.Split(':')[0];
                    if (!sessionNames.Contains(configName) && !recordingDevices.Contains(configName))
                    {
                        newConfigs.Add(config);
                    }
                }
            }

            EvalestMidiatorKeyboard.Properties.Settings.Default.Config = newConfigs;
            EvalestMidiatorKeyboard.Properties.Settings.Default.Save();
        }

        private void RemoveControl_Click(object sender, EventArgs e)
        {
            if (activeApps.SelectedItems.Count > 0)
            {
                for (int i = 0; i < activeAppsChanges.Count; i++)
                {
                    if (activeAppsChanges[i].Application == (activeApps.SelectedItem as App).Application)
                    {
                        (activeApps.Items[i] as App).AKey = "";
                        sessionControls[i] = "";
                    }
                }
            }
            if (activeMics.SelectedItems.Count > 0)
            {
                for (int i = 0; i < activeMicsChanges.Count; i++)
                {
                    if (activeMicsChanges[i].Microphone == (activeMics.SelectedItem as Mic).Microphone)
                    {
                        (activeMics.Items[i] as Mic).MKey = "";
                        recordingControls[i] = "";
                    }
                }
            }
        }

        private void MidiController_Resize(object sender, EventArgs e)
        {
            if (WindowState == WindowState.Minimized)
            {
                Hide();
                ShowInTaskbar = false;
                System.Windows.Forms.NotifyIcon ni = new System.Windows.Forms.NotifyIcon();
                ni.Icon = new System.Drawing.Icon("Main.ico");
                ni.Visible = true;
                ni.DoubleClick += delegate (object Sender, EventArgs args)
                    {
                        this.Show();
                        this.WindowState = WindowState.Normal;
                    };
            }
        }

        private void CheckBox_Checked(object sender, EventArgs e)
        {
            RegistryKey rk = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            rk = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            rk.SetValue("EvalestMidiatorKeyboard", System.Windows.Forms.Application.ExecutablePath);
        }

        public void CheckBox_Unchecked(object sender, EventArgs e)
        {
            RegistryKey rk = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            rk = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            rk.DeleteValue("EvalestMidiatorKeyboard", false);
        }

        private void startUpCheck()
        {
            RegistryKey rk = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            if (rk.GetValue("EvalestMidiatorKeyboard") == null)
            {
                checkStart.IsChecked = false;
            }
            else
            {
                checkStart.IsChecked = true;
            }
        }

        private void activeApps_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (activeMics.SelectedItems.Count > 0)
            {
                activeMics.SelectedItem = null;
            }
            userSelection();
        }

        private void activeMics_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (activeApps.SelectedItems.Count > 0)
            {
                activeApps.SelectedItem = null;
            }
            userSelection();
        }

        public void move_window(object sender, MouseButtonEventArgs e)
        {
            ReleaseCapture();
            SendMessage(new WindowInteropHelper(this).Handle,
                WM_NCLBUTTONDOWN, HT_CAPTION, 0);
        }

        private void EXIT(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void MINIMIZE(object sender, MouseButtonEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        public void Activate_Title_Icons(object sender, System.Windows.Input.MouseEventArgs e)
        {
            Close_btn.Fill = (ImageBrush)Main.Resources["Close_act"];
        }

        private void Deactivate_Title_Icons(object sender, System.Windows.Input.MouseEventArgs e)
        {
            Close_btn.Fill = (ImageBrush)Main.Resources["Close_inact"];
        }

        private void Close_pressing(object sender, MouseButtonEventArgs e)
        {
            Close_btn.Fill = (ImageBrush)Main.Resources["Close_pr"];
        }

        private void findExePath()
        {
            RegistryKey rk = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            try
            {
                exePath = System.Reflection.Assembly.GetExecutingAssembly().Location.Replace("\\EvalestMidiatorKeyboard.exe", "");
            }
            catch
            {
                exePath = @".\";
            }
        }
    }
}